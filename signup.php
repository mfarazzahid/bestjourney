﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="signup.aspx.cs" Inherits="signup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .auto-style1 {
            width: 365px;
        }
        .auto-style2 {
            width: 365px;
            height: 20px;
        }
        .auto-style3 {
            height: 20px;
        }
        .auto-style4 {
            width: 365px;
            height: 70px;
        }
        .auto-style5 {
            height: 70px;
        }
    </style>
</head>
<body>
     <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">Best Journey</a>
          </div>
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.html">Home</a></li>
            <li><a href="index.html#aboutus">About</a></li>
            <li><a href="index.html#contactus">Contact</a></li>
            <li><a href="hotels.aspx">Hotels</a></li>
            <li><a href="flights.aspx">Flights</a></li> 
            <li><a href="#">Get Offer</a></li>
            <li><a href="#">Brands</a></li>
            <li><a href="#">Terms & Conditions</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="signup.aspx"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            <li><a href="login.aspx"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
          </ul>
        </div>
      </nav>
    
    <form id="form1" runat="server">
    <div class="container-fluid">
        <div class="col-xs-2"></div>
        <div class="col-md-8">
            <div class="container" dir="auto" style="border-color: #00CCFF; background-color: #FFFFFF; overflow: auto">

                <asp:Label ID="Label1" runat="server"  BorderColor="#0000CC" Font-Bold="True" Font-Names="Adobe Caslon Pro Bold" Font-Size="X-Large" ForeColor="#3399FF" TabIndex="2" Text="SignUp"></asp:Label>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <table class="nav-justified">
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label2" runat="server" Text="Full Name" CssClass="form-control" ForeColor="Blue" BorderColor="Blue"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server" Width="285px" CssClass="input-lg" AutoCompleteType="DisplayName" BorderColor="Aqua"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2"></td>
                <td class="auto-style3"></td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label3" runat="server" Text="Email Address" CssClass="form-control" ForeColor="Blue" BorderColor="Blue"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server" Width="282px" CssClass="input-lg" AutoCompleteType="Email" BorderColor="Aqua"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label4" runat="server" Text="Password" CssClass="form-control" ForeColor="Blue" BorderColor="Blue"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server" Width="285px" CssClass="input-lg" BorderColor="Aqua"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label5" runat="server" Text="Mobile Number" CssClass="form-control" ForeColor="Blue" BorderColor="Blue"></asp:Label>
                </td>
                <td class="auto-style5">
                    <asp:TextBox ID="TextBox4" runat="server" Width="287px" CssClass="input-lg" AutoCompleteType="Cellular" BorderColor="#0099FF"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label6" runat="server" Text="Address" CssClass="form-control" ForeColor="Blue" BorderColor="Blue"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox5" runat="server" Width="287px" CssClass="input-lg" AutoCompleteType="HomeStreetAddress" BorderColor="Aqua"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="SignUp" Width="160px" BackColor="Lime" CssClass="bg-success" Font-Bold="True" Font-Names="BatangChe" Font-Size="Large" Height="47px" />
                </td>
            </tr>
        </table>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
        
    </form>
</body>
</html>
