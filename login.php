﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .auto-style1 {
            width: 218px
        }
        .auto-style2 {
            width: 218px;
            height: 20px;
        }
        .auto-style3 {
            height: 20px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">Best Journey</a>
          </div>
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.html">Home</a></li>
            <li><a href="index.html#aboutus">About</a></li>
            <li><a href="index.html#contactus">Contact</a></li>
            <li><a href="hotels.aspx">Hotels</a></li>
            <li><a href="flights.aspx">Flights</a></li> 
            <li><a href="#">Get Offer</a></li>
            <li><a href="#">Brands</a></li>
            <li><a href="#">Terms & Conditions</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="signup.aspx"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            <li><a href="login.aspx"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
          </ul>
        </div>
      </nav>




    </div>
        <asp:Label ID="Label1" runat="server" CssClass="label-warning" Font-Bold="True" Font-Italic="True" Font-Names="BatangChe" Font-Size="X-Large" Font-Underline="True" ForeColor="Blue" Text="Login Form" Visible="False"></asp:Label>
        
            <table align="center" class="nav-justified" border="0" style="background-color: #FFFFFF">
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label2" runat="server" CssClass="form-control" Font-Bold="True" Font-Italic="True" Font-Names="BatangChe" ForeColor="Blue" Text="Username/email"></asp:Label>
                </td>
                <td class="auto-style3">
                    <asp:TextBox ID="TextBox1" runat="server" Width="242px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label3" runat="server" CssClass="form-control" Font-Bold="True" Font-Italic="True" Font-Names="BatangChe" ForeColor="Blue" Height="34px" Text="Password" Width="217px"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server" Width="242px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td>
                    <asp:Button ID="Button1" runat="server" BackColor="Blue" CssClass="bg-success" Font-Bold="True" Font-Italic="True" Font-Names="BatangChe" Font-Size="X-Large" ForeColor="White" Text="Login" Width="154px" />
                </td>
            </tr>
        </table>
        
    </form>
</body>
</html>
