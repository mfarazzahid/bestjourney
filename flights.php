﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="flights.aspx.cs" Inherits="flights" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .auto-style1 {
            width: 363px;
        }
        .auto-style2 {
            width: 363px;
            height: 29px;
        }
        .auto-style3 {
            height: 29px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">Best Journey</a>
          </div>
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.html">Home</a></li>
            <li><a href="index.html#aboutus">About</a></li>
            <li><a href="index.html#contactus">Contact</a></li>
            <li><a href="hotels.aspx">Hotels</a></li>
            <li><a href="flights.aspx">Flights</a></li> 
            <li><a href="#">Get Offer</a></li>
            <li><a href="#">Brands</a></li>
            <li><a href="#">Terms & Conditions</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="signup.aspx"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            <li><a href="login.aspx"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
          </ul>
        </div>
      </nav>

        <div class="container-fluid">
             <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
    
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <img src="images/a1.jpg" width="100%" height="250px" >
        </div>
    
        <div class="item">
          <img src="images/a2.jpg" width="100%" height="250px">
        </div>
    
        <div class="item">
          <img src="images/a3.jpg" width="100%" height="250px">
        </div>

        <div class="item">
          <img src="images/a4.jpg" width="100%" height="250px">
        </div>

        <div class="item">
          <img src="images/.jpg" width="100%" height="250px">
        </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

      </div>
    </div>

        </div>
        <div class="container-fluid">
            
               
            <br />
            
            <br />
            <table class="nav-justified" style="text-align:center">
                <tr>
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Italic="True" Font-Names="David" Font-Size="XX-Large" Text="Find the Flight As Per your Ease"></asp:Label>
                </tr>
                 <tr>
                    <td class="auto-style1" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize; background-color: #FF3300;">&nbsp;</td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize; background-color: #FF3300;">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">
                        <asp:Label ID="from" runat="server" Text="From"></asp:Label>
                    </td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize" class="auto-style3">
                        <asp:TextBox ID="TextBox1" runat="server" Width="187px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">&nbsp;</td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">
                        <asp:Label ID="Label3" runat="server" Text="To"></asp:Label>
                    </td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">
                        <asp:TextBox ID="TextBox2" runat="server" Width="187px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">&nbsp;</td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">
                        <asp:Label ID="Label4" runat="server" Text="Depart"></asp:Label>
                    </td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">
                        <asp:TextBox ID="TextBox3" runat="server" Width="187px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">&nbsp;</td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">
                        <asp:Label ID="Label5" runat="server" Text="Return"></asp:Label>
                    </td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">
                        <asp:TextBox ID="TextBox4" runat="server" Width="187px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">&nbsp;</td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">
                        <asp:Label ID="Label6" runat="server" Text="Cabin Class &amp; Travellers"></asp:Label>
                    </td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">
                        <asp:TextBox ID="TextBox5" runat="server" Width="187px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">&nbsp;</td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">
                        <asp:Label ID="Label7" runat="server" Text="Type"></asp:Label>
                    </td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">
                        <asp:DropDownList ID="DropDownList1" runat="server">
                            <asp:ListItem>Return</asp:ListItem>
                            <asp:ListItem>MultiCty</asp:ListItem>
                            <asp:ListItem>One Way</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">&nbsp;</td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize; background-color: #FF3300;">&nbsp;</td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize; background-color: #FF3300;">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1" style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">&nbsp;</td>
                    <td style="font-family: cursive; font-size: larger; font-weight: bolder; font-style: italic; font-variant: normal; text-transform: capitalize">&nbsp;</td>
                </tr>
            </table>
            <br />
            
               
        </div>


        <footer class="page-footer font-small blue" style="background-color: black;color: white">

          <!-- Copyright -->
          <div class="footer-copyright text-center py-3">© 2018 Copyright:
            <a href="index.html">Bestjourney.com</a>
          </div>
          <!-- Copyright -->
        
        </footer>
    </div>
    </form>
</body>
</html>
