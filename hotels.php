﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="hotels.aspx.cs" Inherits="hotels" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .auto-style1 {
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="#">Best Journey</a>
          </div>
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.html">Home</a></li>
            <li><a href="index.html#aboutus">About</a></li>
            <li><a href="index.html#contactus">Contact</a></li>
            <li><a href="hotels.aspx">Hotels</a></li>
            <li><a href="flights.aspx">Flights</a></li> 
            <li><a href="#">Get Offer</a></li>
            <li><a href="#">Brands</a></li>
            <li><a href="#">Terms & Conditions</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="signup.aspx"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            <li><a href="login.aspx"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
          </ul>
        </div>
      </nav>
    </div>

    <div class="container-fluid">
             <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
    
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <img src="images/movenpick.jpg" width="100%" height="250px" >
        </div>
    
        <div class="item">
          <img src="images/ramadacreek.jpg" width="100%" height="250px">
        </div>
    
        <div class="item">
          <img src="images/monal.jpg" width="100%" height="250px">
        </div>

        <div class="item">
          <img src="images/rooms.jpg" width="100%" height="250px">
        </div>

        <div class="item">
          <img src="images/luxurious.jpg" width="100%" height="250px">
        </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

      </div>
    </div>
        
    <div class="container-fluid">
        <div  style="text-align:center" ><a href="#details"><br />For Your Choice<br /><img src="images/downward.png" /></a></div>
    </div>

    <div class="container-fluid" id="details">
            <div class="jumbotron">
                <div class="label-danger" style="text-align:center;margin-top:10%; font-family: BatangChe; font-size: larger; font-weight: 600; font-style: italic; font-variant: normal; text-transform: capitalize; color: #000000;"><h4 style="font-family: cursive; font-size: larger"><br />Search The Best Hotel</h4>
                    
                    <table class="nav-justified">
                        <br /><br />
                        <tr>
                            <td class="auto-style1" style="font-family: cursive; font-size: larger; background-color: #FFFFFF;">
                                <asp:Label ID="Label1" runat="server" Text="City"></asp:Label>
                            </td>
                            <td style="font-family: cursive; font-size: larger; background-color: #FFFFFF">
                                <asp:TextBox ID="citytext" runat="server" Width="187px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1" style="font-family: cursive; font-size: larger; background-color: #FFFFFF;">&nbsp;</td>
                            <td style="font-family: cursive; font-size: larger; background-color: #FFFFFF">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style1" style="font-family: cursive; font-size: larger; background-color: #FFFFFF;">
                                <asp:Label ID="Label2" runat="server" Text="Check In"></asp:Label>
                            </td>
                            <td style="font-family: cursive; font-size: larger; background-color: #FFFFFF">
                                <asp:TextBox ID="checkintext" runat="server" Width="187px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1" style="font-family: cursive; font-size: larger; background-color: #FFFFFF;">&nbsp;</td>
                            <td style="font-family: cursive; font-size: larger; background-color: #FFFFFF">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style1" style="font-family: cursive; font-size: larger; background-color: #FFFFFF;">
                                <asp:Label ID="Label3" runat="server" Text="Check Out"></asp:Label>
                            </td>
                            <td style="font-family: cursive; font-size: larger; background-color: #FFFFFF">
                                <asp:TextBox ID="checkouttext" runat="server" Width="187px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1" style="font-family: cursive; font-size: larger; background-color: #FFFFFF;">&nbsp;</td>
                            <td style="font-family: cursive; font-size: larger; background-color: #FFFFFF">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style1" style="font-family: cursive; font-size: larger; background-color: #FFFFFF;">
                                <asp:Label ID="Label4" runat="server" Text="Rooms"></asp:Label>
                            </td>
                            <td style="font-family: cursive; font-size: larger; background-color: #FFFFFF">
                                <asp:TextBox ID="roomstext" runat="server" Width="187px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1" style="font-family: cursive; font-size: larger; background-color: #FFFFFF;">&nbsp;</td>
                            <td style="font-family: cursive; font-size: larger; background-color: #FFFFFF">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style1" style="font-family: cursive; font-size: larger; background-color: #FFFFFF;">
                                <asp:Label ID="Label5" runat="server" Text="Guest"></asp:Label>
                            </td>
                            <td style="font-family: cursive; font-size: larger; background-color: #FFFFFF">
                                <asp:TextBox ID="guesttext" runat="server" Width="187px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1" style="font-family: cursive; font-size: larger; background-color: #FFFFFF;">&nbsp;</td>
                            <td style="font-family: cursive; font-size: larger; background-color: #FFFFFF">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style1" style="font-family: cursive; font-size: larger; background-color: #FFFFFF;">
                                <asp:Label ID="Label6" runat="server" Text="Cost "></asp:Label>
                            </td>
                            <td style="font-family: cursive; font-size: larger; background-color: #FFFFFF">
                                <asp:TextBox ID="costtext" runat="server" Width="187px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1" style="font-family: cursive; font-size: larger; background-color: #FFFFFF;">&nbsp;</td>
                            <td style="font-family: cursive; font-size: larger; background-color: #FFFFFF">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style1" style="font-family: cursive; font-size: larger; background-color: #FFFFFF;" colspan="2">
                                <asp:Button ID="Button1" runat="server" CssClass="btn-danger" Font-Italic="True" Font-Names="Charlemagne Std" Text="Search" Width="137px" OnClick="Button1_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>   
    </div>
           <div class="container">
               <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" CellPadding="4" Height="171px" Width="953px">
                   <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                   <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
                   <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                   <RowStyle BackColor="White" ForeColor="#003399" />
                   <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                   <SortedAscendingCellStyle BackColor="#EDF6F6" />
                   <SortedAscendingHeaderStyle BackColor="#0D4AC4" />
                   <SortedDescendingCellStyle BackColor="#D6DFDF" />
                   <SortedDescendingHeaderStyle BackColor="#002876" />
               </asp:GridView>
           </div>
    </form>

      <footer class="page-footer font-small blue" style="background-color: black;color: white">

          <!-- Copyright -->
          <div class="footer-copyright text-center py-3">© 2018 Copyright:
            <a href="index.html">Bestjourney.com</a>
          </div>
          <!-- Copyright -->
        
        </footer>
</body>
</html>
